#include <DynamicHID.h>
#include <Joystick.h>

#include <Wire.h>
#include "Adafruit_MCP23017.h"

bool printAxisState = false;
bool printButtonState = true;


Adafruit_MCP23017 mcp;
Joystick_ Joystick;

#define XAXIS A2
#define YAXIS A3
#define YAW   A1
#define ROLL  A0
#define THROTTLE A10


int buttons[] = {4, 8, 7, 14, 5, 6, 9};

int inputs_analog[] = {512, 512, 512, 512, 512};
int inputs_digital[7];
int inputs_digital_prev[7];

int inputs_throttle_digital[16];
int inputs_throttle_digital_prev[16];

void setup() {
  // put your setup code here, to run once:
  pinMode(XAXIS, INPUT);
  pinMode(YAXIS, INPUT);
  pinMode(YAW, INPUT);

  pinMode(ROLL, INPUT);
  pinMode(THROTTLE, INPUT);

  for (int i = 0; i < 7; i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }

  mcp.begin();      // use default address 0
  for (int i = 0; i < 16; i++) {
    mcp.pinMode(i, INPUT);
    mcp.pullUp(i, HIGH);  // turn on a 100K pullup internally
  }

  Joystick.setXAxisRange(-127, 127);
  Joystick.setYAxisRange(-127, 127);
  Joystick.setZAxisRange(-127, 127);
  Joystick.setThrottleRange(0, 255);
  Joystick.setRyAxisRange(0, 360);

  Joystick.begin();

  Serial.begin(115200);
}

void loop() {
  UpdateButtons();


  Joystick.setXAxis(map(analogRead(XAXIS), 666 - 20, 376 + 20, -127, 127));



  Joystick.setYAxis(map(analogRead(YAXIS), 666 - 20, 376 + 20, -127, 127));

  int yaw = analogRead(YAW);
  if (yaw > 486 - 20 && yaw < 486 + 26) {
    yaw = 486;
  } else if (yaw < 486) {
    yaw += 20;
  } else if (yaw > 486) {
    yaw -= 20;
  }

  Joystick.setZAxis(map(yaw, 486 - 120, 486 + 120, -127, 127));



  Joystick.setRyAxis(map(analogRead(THROTTLE), 325, 740, 0, 255));

  yaw = analogRead(ROLL);
  if (yaw > 510 - 20 && yaw < 510 + 20) {
    yaw = 510;
  } else if (yaw < 510) {
    yaw += 20;
  } else if (yaw > 510) {
    yaw -= 20;
  }

  Joystick.setThrottle(map(yaw, 680, 320, 0, 360));


  if (printAxisState) {
    Serial.print(1024 - analogRead(XAXIS));
    Serial.print("\t");
    Serial.print(analogRead(YAXIS));
    Serial.print("\t");
    Serial.print(analogRead(YAW));
    Serial.print("\t");
    Serial.print(analogRead(ROLL));
    Serial.print("\t");
    Serial.print(1024 - analogRead(THROTTLE));
    Serial.print("\t");


    Serial.println();
  }

  Joystick.sendState();
  delay(10);
}

void OnButtonDown(int buttonIdx) {
  Joystick.pressButton(buttonIdx);

  if (printButtonState) {
    Serial.print(buttonIdx);
    Serial.println(" button pressed");
  }
}

void OnButtonUp(int buttonIdx) {
  Joystick.releaseButton(buttonIdx);
  if (printButtonState) {
    Serial.print(buttonIdx);
    Serial.println(" button released");
  }
}

void UpdateButtons() {
  for (int i = 0; i < 7; i++) {
    inputs_digital[i] = digitalRead(buttons[i]);
    if (!inputs_digital[i] && inputs_digital_prev[i]) {
      OnButtonDown(i);
    }
    if (inputs_digital[i] && !inputs_digital_prev[i]) {
      OnButtonUp(i);
    }
    inputs_digital_prev[i] = inputs_digital[i];
  }

  for (int i = 0; i < 16; i++) {
    inputs_throttle_digital[i] = mcp.digitalRead(i);
    if (!inputs_throttle_digital[i] && inputs_throttle_digital_prev[i]) {
      OnButtonDown(i+7);
    }
    if (inputs_throttle_digital[i] && !inputs_throttle_digital_prev[i]) {
      OnButtonUp(i+7);
    }
    inputs_throttle_digital_prev[i] = inputs_throttle_digital[i];
  }

}
